
# Copyright (C) 2011-2013, Travis Bear
# All rights reserved.
#
# This file is part of Graphite Log Feeder.
#
# Graphite Log Feeder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Graphite Log Feeder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Graphite Log Feeder; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GRAPHITE_SECTION = "graphite"
CARBON_HOST = "carbon_host"
CARBON_INTERVAL_SECONDS = "carbon_interval_seconds"
CARBON_PORT = "carbon_port"
CARBON_PREFIX = "carbon_prefix"
CARBON_SUFFIX = "carbon_suffix"

DATA_SECTION = "data"
LOG_FILE = "log_file"

GRINDER_SECTION="grinder"
TIME_GROUP_MILLISECONDS = "time_group_milliseconds"
GRINDER_MAPPING_FILE = "grinder_mapping_file"
